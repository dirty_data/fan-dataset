# FAN dataset

Repository for the FAN (French Attribution Notices) dataset.

## Context

### Dirty Data Project

This dataset was assembled for the French ANR research project [Dirty Data](https://project.inria.fr/dirtydata/).
Dirty Data stems out of the acknowledgment that most real data is dirty, and
that the availability of high-quality, open-source ML and data analysis
frameworks (such as [scikit-learn](https://scikit-learn.org/),
[pandas](https://pandas.pydata.org/)...) means that the next frontier for
tooling and automation lies in preprocessing.
To answer these challenges, the project aims at developing methodologies to
perform statistical analysis directly on the original dirty data.

For evaluation purposes, a novel gold-standard dataset of real-life data
featuring various types of errors has been put together.
It is meant to be used for inference evaluation under conditions of
reconciliation with reference data and missing data.

### French award notices for public calls for tenders

The data comes from french open data on companies bidding on public calls for
tenders, and from financial declarations.

Every French public organisation has to issue a call for tenders when buying
supplies or services (above a minimum threshold). When the bid is awarded to a
company, a notice has to be legally published by the public organization on
the [BOAMP](https://www.boamp.fr/) (historical data is hosted by the
[DILA](https://www.dila.premier-ministre.gouv.fr/)). At this stage, about
25% of awards are actually electronically published.

The award notices for French public tenders are used as sparse data to predict
company revenues.

This fits well the specific context of the Dirty Data project, since the award
notices data is quite dirty, and has to be reconciled to a reference database
to be usable. It is also incomplete to predict company revenue, and there are
other interesting data quality issues.

The final dataset comprises award notices from 2017 and 2018, as well as company
revenues from 2013 to 2018.

## Content

The dataset consists in two files.

### The award notices file

Notices are related to public procurement contracts, which bidders compete
anonymously on. The contract can be divided into a maximum of 5 lots, and
several companies can issue a common proposal. In the data, each row represents
one award notice (also referred to as a 'call') and the columns provide the
following information about the award notice:

* `ID_call` - ID of the award notice
* `Publication_date` of the award notice
* `End_of_call_date` of the award notice
* `Departments_of_publication` - the department code(s) of the award notice
* `Department_of_provision` - the department code(s) where the contract 
  works/goods/services were provided 
* `Call_summary` - summary of the award notice
* `Call_title` - title of the award notice
* `Complete_call_description` - description of the award notice
* `Total_amount` - total amount of the contract, from all lots, in euros
* `CPV_classes` - Common Procurement Vocabulary (CPV), a classification 
  system for public procurement used to describe the subject of procurement 
  contracts (more information can be found [here](https://simap.ted.europa.eu/cpv))
* `Buyer_name` 
* `Buyer_address`
* `Buyer_zipcode`
* `Buyer_city`
* `Buyer_email`
* `Buyer_URL`
* `Contract_awarded` - whether or not the contact was awarded - all values are
  'yes'

and the following information about EACH lot:

* `ID` - lot ID 
* `awarded` - whether or not the contact was awarded - either 'yes' or `NaN`
* `description` - description of the lot
* `incumbent_name` - name of the lot winnter
* `incumbent_address` - address of the lot winner
* `incumbent_zipcode` - zipcode of the lot winner
* `incumbent_city` - city of the lot winner
* `incumbent_country` - country of the lot winner
* `number_of_received_bids` - number of bids this lot received
* `amount` - amount of the lot in euros

Note that the above columns are prefixed with `Lot_1` to `Lot_5` to identify the
lot to which the column relates to. If there are less than 5 lots for a
particular award notice, the values for the redundant lot columns are `NaN`. 

Notices are originally published in a XML format using a complex schema. This
data has already been cleaned and denormalized to a tabular format, fitting the
scope of the project.

The file is in a UTF-8 encoded, CSV format with 67 columns.
It contains award notices for **76 198** contracts (37 693 in 2017, and 38 505
in 2018), one line representing one award.

For each award notice, a single company can win all of the lots or a lot can be
split between several companies. An awardee is a company listed as having won
a lot of an award notice. There are 125 933 unique awardees after
deduplication on the available columns (incumbent_name, incumbent_address,
incumbent_zipcode, incumbent_city, incumbent_country).

A reconciliation algorithm was used to find the legal ID for these awardees. A
total of 109 932 awardees were matched with a legal ID. After deduplicating on
legal ID there are 39 566 deduplicated companies.

The reconciliation process has a recall of 87.3% and a precision of 95.7%
(manually evaluated on a random sample of 300 matched awardees).
The reconciliated IDs are not published.

### The revenues file

This file is built from an extract of the 
[National Institute of Statistics and Economic Studies (INSEE)](https://www.insee.fr/en/accueil)
reference database of company revenue declarations from 2013 to 2018. The file 
'financial_data_french_entities_cleaned.csv' is a UTF-8 encoded file in CSV
format with 11 columns. It contains 2 839 947 declarations for 1 065 018 unique
companies. Each row represents the declaration of one company for one year and
the following information is provided in the columns:

* `Legal_ID` - the reconcilled legal ID of the company
* `Name` - the name of the company
* `Activity_code (APE)` - 'Activite Principale de l'Entreprise', the main
   activity of the company - more information in [English](https://www.startbusinessinfrance.com/code-ape)
   or in [French](https://www.service-public.fr/professionnels-entreprises/vosdroits/F33050)
* `Address` 
* `Zipcode`
* `City`
* `Revenue` - in Euros
* `Headcount`
* `Fiscal_year_end_date`
* `Fiscal_year_duration_in_months`
* `Year`      

The following cleaning procedures have been performed on the file
'financial_data_french_entities_cleaned.csv':

* Missing addresses and zipcodes (76% and 77% null values respectively) were
  backfilled using the `Legal_ID`'s, allowing removal of duplicated rows 
  (3 966 025 -> 3 073 634, 22% reduction).
* Incomplete revenue declarations (rows) were merged, (3 073 634 -> 2 982 796,
  3% reduction).
* Backfilled activity codes, allowing removal of duplicate rows (2 982 796 -> 
  2 973 478, <1% reduction).
* Normalized text in uppercase, allowing removal of duplicate rows 
  (2 973 478 -> 2 968 474, 2% reduction).
* Deleting typos and other problems, allowing removal of duplicate rows 
  (2 968 474 -> 2 884 929, 3% reduction).
* Merging again incomplete revenue declarations, (2 884 929 -> 2 856 446, 1%
  reduction).
* Deleting more rows due to different zipcodes, (2 856 446 -> 2 849 144, <1%
  reduction).
* For duplicate entries where the `Legal_ID` and company `Name` were the same
  but `Revenue` differed (usually by a small amount) - if the difference
  in `Revenue` was less than 0.1%, only one entry was kept. Otherwise both
  entries were deleted.
* Corrected issues with some companies (at least 3323) where the revenue is
  declared in k€ instead of €, (2 845 324 -> 2 839 947, <1% reduction).

Things to note:

* Revenue declarations are for the years 2013-2018 inclusive. Number of entries
  for each year:
    * 2013.0 - 798 293
    * 2014.0 - 715 133
    * 2015.0 - 679 295
    * 2016.0 - 509 867
    * 2017.0 - 95 021
    * 2018.0 - 42 330
    * (the large reduction in entries for the years 2017 and 2018 are due to
      the Loi Macron law in 2017)
* The median change between years for the same company is 0.02%.
* The 'same company' can have several different entities, resulting in entries
  where the `Legal_ID` is different but the `Name`, `Address`, `City` and
  `Zipcode` are all the same.
* The `Fiscal_year_end_date` varies across companies and even within the same
  company, between different years.
* The `Fiscal_year_duration` is 12 months for ~96% of entries. It varies from 
  -2 to 99 months.
* `Revenue` may be negative or very low. A negative revenue may be due to
  cancelled orders from the previous fiscal year that was recorded in the
  current fiscal year. Revenue is:
   * less than 0€ for 1 733 entries
   * equal to 0€ for 98 964 entries
   * less than 10€ for 100 981 of entries
* For each `Legal_ID` there are between 1 and 9 entries. This is generally due
  to `Revenue` entries for different years. For `Legal_ID`'s which have >6
  entries, this is generally due to duplicate entries for some years.

Upon merging the two datasets using the reconcilled legal IDs:

* There are 26 034 companies (using legal ID as unique identifier) with both an
  entry in the award call and company revenue datasets.
* There are 3 403 companies with at least one revenue entry for 2017 and 2018

## Challenge details

To be included in the challenge, companies must have:

* an entry in both the company and award data sets
   * to ensure this, the legal ID must be present in BOTH data sets
* a revenue entry (i.e. not `NaN`) in the company data set
* fiscal year duration of 12 months (entries with a fiscal year duration
  not equal to 12 months are removed)
* for companies that have revenue entries for >1 year, the revenue should be
  predicted for all years present in the data

We do not have 'ground truth' but are taking the reconcilled legal IDs as
ground truth. As stated above, the reconciliation process has a recall of 87.3%
and a precision of 95.7% (manually evaluated on a random sample of 300 matched
awardees).

### Scoring

![](https://latex.codecogs.com/gif.download?%7Cmax%285%2Clog_%7B10%7D%28min%281%2Cy%5C_true%29%29%29%20-%20max%285%2Clog_%7B10%7D%28min%281%2Cy%5C_pred%29%29%29%7C)

Score interpretation:

* Lower score is better
* If both the `y_true` and `y_pred` are less than 100 000, the score would be
  0.
* The score is the same regardless of the order of `y_true` and `y_pred` in
  the equation.
* If the difference in raw `y_true` and `y_pred` values is the same, the score
  is greater for smaller magnitudes of `y_true` and `y_pred`.

### Cleaning

Steps taken to clean the original datasets:

`award_notices_reconciled_IDs.csv`:

1. Melt such that one lot is one row (instead of up to 5 lots per row).
   Shape: (151 439, 30).
2. Keep only entries where there is a reconcilled Legal ID. Shape (133 209, 30).

Number of unique Legal IDs in awards: 39 557

`financial_data_french_entities_cleaned`:

1. Original shape: (2 839 947, 11).
2. Keep only entries where `Fiscal_year_duration_in_months` == 12.
   Shape: (2 721 421, 11).
3. Keep only entries where `Revenue` is NOT `NaN`. Shape (2 719 095, 11).
4. Ensure there are no duplicate entries (same Legal ID and year) in the test
   dataset:
   1. Find all the `Legal_ID`'s for which there are duplicate entries.
   2. Remove all entries with these 'duplicate Legal IDs'.
   3. Split the data into test and train (grouping by `Legal_ID`), proportion
      in test = 0.26 (due to large number of duplicates, which will be added
      to test).
   4. Add the 'duplicate Legal ID' entries to test subset.

# Data files

## Awards

* `award_notices.csv` - original award notices: (146 972, 67).
* `award_notices_reconciled_IDs.csv` - original award notices with reconcilled
  IDs: (146 972, 72) - there are 5 additional columns cf above as each row
  contains information for 5 lots.
* Full award notices: original award notices melted such that each row
  contains information on one lot (instead of 5 lots).
   * `award_notices_RAMP.csv` -  no Legal IDs: (304 098, 28).
   * `award_notices_RAMP_ID.csv` - with Legal IDs: (304 098, 29)

## Company

* `financial_data_french_entities.csv` - full declared revenue dataset 
  (3 966 025, 11).
* `financial_data_french_entities_cleaned.csv` - full revenue dataset
  with duplicate entries removed (see above) (2 839 947, 11).
* Full train and test: Entries where `Fiscal_year_duration_in_months` were not
  12 months and where `Revenue` was `NaN` were removed. Data was then split
  into train and test, using Legal ID groups so the same company does not
  appear in both train and test:
   * `financial_data_TEST.csv` - (702 181, 11).
   * `financial_data_TRAIN.csv` - (2 016 914, 11).
* The private training data `financial_data_TRAIN.csv` is also used as public
  training data. This is again split into public training and public test.
   * `financial_data_public_TEST.csv` - (520 966, 11).
   * `financial_data_public_TRAIN.csv` - (1 495 948, 11).